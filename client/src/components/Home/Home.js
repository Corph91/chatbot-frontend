import React from 'react';
import './styles.css'

const Home = (props) => {
  return(
    <div className={"container main"}>
      <div className="row item-row top-sec">
      </div>
      <div className={"row item-row chat-row"}>
        <div className="col-md-12 input-sec">
          <textarea type="text" placeholder="Enter your code question!" onChange={(event) => props.changeText(event)}/>
        </div>
        <div className="col-md-12 button-sec">
          <button type="button" className="btn btn-primary" onClick={(event) => props.submitQuestion(event)}>Ask!</button>
        </div>
        <div className="col-md-12 answer-sec">
          <label>Chatbot says:</label>
          <textarea className="response" value={(props.answer) ? props.answer : ""}
          disabled
          autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck="false"></textarea>
        </div>
      </div>
      <div className="row item-row bottom-sec">
      </div>
    </div>
  )
}

export default Home
