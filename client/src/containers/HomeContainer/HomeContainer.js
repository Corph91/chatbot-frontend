import React, {Component} from 'react';
import {Home} from '../../components'
import $ from 'jquery';

class HomeContainer extends Component{
  state = {
    question: undefined,
    answer: undefined
  }
  changeText = (event) => this.setState({question: event.target.value})
  submitQuestion = this.submitQuestion.bind(this)
  submitQuestion(event){
    event.preventDefault();
    console.log("Submitting question",this.state.question);
    $.ajax({
      url:'/chat-api/post',
      method:'POST',
      data: {question: this.state.question}
    }).done((response) => {
      console.log("Responding",response);
      this.setState({answer: response.data})
    });
  }
  render(){
    return(
      <Home
      changeText={this.changeText}
      submitQuestion={this.submitQuestion}
      answer={this.state.answer}
      />
    )
  }
}

export default HomeContainer
