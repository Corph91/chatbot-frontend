import React from 'react';
import ReactDOM from 'react-dom';
import {Router,
        Route,
        browserHistory,
        IndexRoute}
        from 'react-router'

import App from './App';
import {HomeContainer} from './containers';
import './App.css'
require('bootstrap/dist/css/bootstrap.css');

ReactDOM.render(
  <Router history={browserHistory} className="outer-main">
    <Route path="/" component={App}>
      <Route path="/home" components={HomeContainer} />
      <IndexRoute component={HomeContainer}/>
    </Route>
  </Router>
    , document.getElementById('root'));
