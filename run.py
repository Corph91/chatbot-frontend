from subprocess import call
import multiprocessing

call_str = ['FLASK_APP=chatbot.py flask run','npm start']

def go(call_str):
    if not call_str == 'npm start':
        call(call_str,shell=True)
    else:
        call(call_str,shell=True, cwd="./client")

def init():
    if __name__ == '__main__':
        print("GO TIME!")
        pool = multiprocessing.Pool(processes=2)
        pool.map(go, call_str)

init()
