from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
from models.question import Question as Question
#import tensorflow as tf
import execute

app = Flask('chatbot')
mongo = PyMongo(app)

@app.route("/")
def hello():
    return "Hello world!"

@app.route("/chat-api/post",methods=["POST"])
def post_question():
    newQuestion = Question(text=request.form['question'])
    newQuestion.save()
    #execute.decode_line(sess, model, enc_vocab, rev_dec_vocab, request.form["question"])
    return jsonify({'data': "Back atcha! I hope all is going well! This is a test of an answer that's really long and should wrap but probably won't I hope this works because if it doesn't I have to spend forever changing this! Wow! It works! What can I say, you smartie pants! Wow! Nice work! I hope this paragraph of text makes you feel awesome and causes some scrolling."})
